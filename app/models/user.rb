class User < ActiveRecord::Base

  validates :password_digest, :username, presence: true
  validates :username, uniqueness: true

  def generate_session_token!
    self.session_token = SecureRandom.urlsafe_base64(20)
    self.save!
  end

  def password=(plain_text)
    self.password_digest = BCrypt::Password.create(plain_text)
  end

  def is_password?(plain_text)
    BCrypt::Password.new(self.password_digest).is_password?(plain_text)
  end

  def self.find_by_credentials(name, pw)
    user = User.find_by_username(name)
    if user && user.is_password?(pw)
      return user
    end
    return nil
  end

end
