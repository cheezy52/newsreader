module SessionsHelper
  def current_user
    @current_user ||= User.find_by_session_token(session[:session_token])
  end

  def logged_in?
    !!current_user
  end

  def login(user)

    user.generate_session_token!
    session[:session_token] = user.session_token
  end

  def logout
    session[:session_token] = nil
    current_user.generate_session_token!
  end
end
