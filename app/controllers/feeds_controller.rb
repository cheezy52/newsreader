class FeedsController < ApplicationController
  before_action :verify_logged_in

  def index
    respond_to do |format|
      format.html { render :index }
      format.json { render :json => Feed.all, include: :entries }
    end
  end

  def create
    feed = Feed.find_or_create_by_url(feed_params[:url])
    feed.reload if (feed.updated_at && feed.updated_at <= 2.minutes.ago)
    if feed
      render :json => feed, include: :entries
    else
      render :json => { error: "invalid url" }, status: :unprocessable_entity
    end
  end

  def show
    feed = Feed.find(params[:id])
    feed.reload if (feed.updated_at && feed.updated_at <= 2.minutes.ago)
    if feed
      render :json => feed, include: :entries
    else
      render :json => { error: "Feed not found with id of #{params[:id]}" }, status: 404
    end
  end

  private
  def feed_params
    params.require(:feed).permit(:title, :url)
  end

  def verify_logged_in
    redirect_to new_session_url unless logged_in?
  end
end
