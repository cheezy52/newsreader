class SessionsController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.find_by_credentials(user_params[:username], user_params[:password])
    if @user
      login(@user)
      redirect_to root_url
    else
      flash.now[:errors] = @user.errors.full_messages
      render :new
    end
  end

  def destroy
    if current_user
      logout
      redirect_to new_session_url
    end
  end

  def show
    if current_user
      render :json => @user.to_json(only: [:username, :id])
    else
      head 404
    end
  end

  private
  def user_params
    params.require(:user).permit(:username, :password)
  end
end
