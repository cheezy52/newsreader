class EntriesController < ApplicationController
  before_action :verify_logged_in

  def index
    feed = Feed.find(params[:feed_id])
    render :json => feed.entries
  end

  def show
    entry = Entry.find(params[:id])
    if entry
      render :json => entry
    else
      head 404
    end
  end

  private
  def entry_params
    params.require(:entry).permit(:guid, :link, :published_at, :title, :json, :feed_id)
  end

  def verify_logged_in
    redirect_to new_session_url unless logged_in?
  end
end
