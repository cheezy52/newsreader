NewReader.Views.FeedFormView = Backbone.View.extend({
	template: JST["FeedForm"],

	events: {
		'submit form': 'submitNewFeed'
	},

	render: function(resp) {
		var renderedContent = this.template({ feed: this.model });
		this.$el.empty();
		this.$el.append(renderedContent);
		if (resp){
			this.$el.prepend(resp.responseText);
		}
		return this;
	},

	submitNewFeed: function(event){
		event.preventDefault();
		var that = this;

		var formData = $(event.target).serializeJSON();
		this.collection.create(formData["feed"], {
			success: function(model, resp){
				that.model = new NewReader.Models.Feed();
				that.render(resp);
			},
			error: function(model, resp){
				that.model = model;
				that.render(resp);
			}

		});


	}
});