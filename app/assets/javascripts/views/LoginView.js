NewReader.Views.LoginView = Backbone.View.extend({
	template: JST["Login"],

	events: {

	},

	render: function() {
		var renderedContent = this.template({ feeds: this.collection });
		this.$el.empty();
		this.$el.append(renderedContent);
		return this;
	}
});