NewReader.Views.FeedsIndexView = Backbone.View.extend({
	template: JST["FeedsIndex"],

	initialize: function() {
		this.listenTo(this.collection, "add change:title remove sync", this.render);
	},

	events: {

	},

	render: function() {
		var renderedContent = this.template({ feeds: this.collection });
		this.$el.empty();
		this.$el.append(renderedContent);
		return this;
	}
});