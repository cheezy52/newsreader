NewReader.Views.EntryView = Backbone.View.extend({
	template: JST["Entry"],

	initialize: function(){
		this.listenTo(this.model, 'sync', this.render)
	},

	render: function() {
		// console.log("(R-E)-" + this.model.id + this.model.get('title'));
		var renderedContent = this.template({ entry: this.model });
		this.$el.empty();
		this.$el.append(renderedContent);
		return this;
	}
})