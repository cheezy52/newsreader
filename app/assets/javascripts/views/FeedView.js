NewReader.Views.FeedView = Backbone.View.extend({
	template: JST["Feed"],

	initialize: function() {
		this.listenTo(this.collection, "all", this.populateEntryViews);
		this.listenTo(this.model, "all", this.render);
		// this.listenTo(this.model, "remove", this.goToIndex);

		this.entryViews = [];
		this.populateEntryViews();
	},

	goToIndex: function() {
		Backbone.history.navigate("#/")
	},

	events: {
		"click button": "refresh"
	},

	refresh: function(event) {
		this.model.fetch();
	},


	populateEntryViews: function(){
		var that = this;
		this.entryViews.forEach(function(view) {
			view.remove();
		});
		this.entryViews = [];

		this.model.entries().each(function(entry){
			var entView = new NewReader.Views.EntryView({model: entry});
			that.entryViews.push(entView);
		});
	},

	render: function() {
		var renderedContent = this.template({ feed: this.model });
		var that = this;

		this.$el.empty();
		this.$el.append(renderedContent);

		this.entryViews.forEach(function(view) {
			$("#feed-entries").prepend(view.render().$el);
		})
		return this;
	},

	remove: function() {
		this.entryViews.forEach(function(view) {
			view.remove();
		});
		Backbone.View.prototype.remove.apply(this);
	}
});