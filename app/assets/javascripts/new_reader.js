window.NewReader = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    var feeds = window.NewReader.Collections.feeds = new NewReader.Collections.Feeds();
		feeds.fetch();

		var currentSession = window.NewReader.Models.currentSession = new NewReader.Models.Session();
		var currentUser = window.NewReader.Models.currentUser = null;
		currentSession.fetch({
			success: function(model) {
				currentUser = new NewReader.Models.User(model);
			},
			error: function() {
				currentUser = null;
			}
		});
		var loginBar = new NewReader.Views.LoginView({ model: currentUser });

		this.router = new NewReader.Routers.RSSAppRouter({ $rootEl: $("#content") });
		Backbone.history.start();
  }
};

$(document).ready(function(){
  NewReader.initialize();
});
