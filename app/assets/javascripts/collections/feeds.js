NewReader.Collections.Feeds = Backbone.Collection.extend({
	url: '/feeds',
	model: NewReader.Models.Feed,

	getOrFetch: function(id) {
		if (this.get(id)) {
			this.get(id).fetch();
			return this.get(id);
		}else{
			var that = this;
			var feed = new NewReader.Models.Feed({ id: id });
			feed.fetch({
				success: function(model) {
					that.add(model);
				}
			});
			return feed;
		}
	}
})