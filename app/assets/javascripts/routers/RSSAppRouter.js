NewReader.Routers.RSSAppRouter = Backbone.Router.extend({
	initialize: function(options) {
		this.$rootEl = options.$rootEl;
	},

	routes: {
		"": "feedsIndex",
		"feeds": "feedsIndex",
		"feeds/new" : "newFeed",
		"feeds/:id" : "showFeed",
		"entries/:id": "showEntry"
	},

	feedsIndex: function() {
		var view = new NewReader.Views.FeedsIndexView({ collection: NewReader.Collections.feeds });
		this._swapView(view);
	},

	newFeed: function() {
		var view = new NewReader.Views.FeedFormView({
			model: new NewReader.Models.Feed(),
			collection: NewReader.Collections.feeds
		});
		this._swapView(view);
	},

	showFeed: function(id) {
		var feed = NewReader.Collections.feeds.getOrFetch(id);
		feed.fetch()
		var view = new NewReader.Views.FeedView({
			collection: feed.entries(),
			model: feed
		});
		this._swapView(view);
	},

	showEntry: function(id) {
		var entry = new NewReader.Models.Entry({id: id});
		entry.fetch();
		var view = new NewReader.Views.EntryView({
			model: entry
		});

		this._swapView(view);
	},

	_swapView: function(view) {
		if(this.currentView) {
			this.currentView.remove();
		}
		this.currentView = view;
		this.$rootEl.html(view.render().$el);
	}


})