NewReader::Application.routes.draw do
  resources :feeds, only: [:index, :create, :show] do
    resources :entries, only: [:index]
  end

  resources :entries, only: [:show]

  resources :users, only: [:create, :new]
  resource :session, only: [:create, :new, :destroy, :show]


  root to: "feeds#index"
end
